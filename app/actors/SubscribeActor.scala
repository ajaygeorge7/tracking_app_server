package actors

import akka.actor.{ActorLogging, ActorRef, Props, Actor}
import com.sandinh.paho.akka._

/**
  * Created by ajayg on 10/16/2016.
  */


class SubscribeActor extends Actor with ActorLogging {

  val pubsub = context.actorOf(Props(classOf[MqttPubSub], PSConfig(
    brokerUrl = "tcp://localhost:1883", //all params is optional except brokerUrl
    userName = null,
    password = null
  )))

  pubsub ! Subscribe("test", self)

  def receive = {
    case SubscribeAck(Subscribe(topic, ref_self, _), fail) =>
      if (fail.isEmpty) context become ready

      else log.error(fail.get, s"Can't subscribe to $topic")
  }

  def ready: Receive = {
    case msg: Message =>
      log.error(msg.topic + " " + msg.payload.mkString)
  }
}
