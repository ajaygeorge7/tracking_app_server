package controllers

import java.util.UUID
import javax.inject.Inject

import actors.WebSocketDashboardActor
import actors.classifications.MessageBus
import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.api.services.AvatarService
import com.mohiva.play.silhouette.api.util.{Clock, PasswordHasher}
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import com.mohiva.play.silhouette.impl.providers.{CredentialsProvider, SocialProviderRegistry}
import forms._
import models.{Vehicle, Organization, User}
import models.services.{VehicleService, OrganizationService, UserService}
import play.api.Configuration
import play.api.Play.current
import play.api.i18n.MessagesApi
import play.api.mvc.{Result, RequestHeader, Action, WebSocket}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class VehicleController @Inject()(
                                   val messagesApi: MessagesApi,
                                   messageBus: MessageBus,
                                   val env: Environment[User, CookieAuthenticator],
                                   organizationService: OrganizationService,
                                   vehicleService: VehicleService,
                                   userService: UserService)
  extends Silhouette[User, CookieAuthenticator] {


  def create = SecuredAction.async { implicit request =>
    CreateVehicleForm.form.bindFromRequest.fold(
      form => Future.successful(BadRequest(views.html.vehicles.vehicleNew(form, form.errorsAsJson.toString(), request.identity))),
      data => {
        val org = organizationService.getOrganizationForUserWithName(request.identity.userID.toString, data.orgName) flatMap {
          case Some(o) =>
            Future.successful(o)
          case None =>
            organizationService.createOrganization(Organization(id = UUID.randomUUID().toString, name = data.orgName), adminUser = request.identity) map(o => o.organization.get) // throw error if not found
        }
        org flatMap { o =>
          vehicleService.createVehicle(Vehicle(UUID.randomUUID().toString, Some(data.name)), o) map { v =>
            Redirect(routes.VehicleController.details(o.id))
          }
        }
      }
    )
  }

  def list(pageNumber: Option[Int], numberOfRecords: Option[Int]) = SecuredAction.async { implicit request =>
    vehicleService.getVehiclesForUser(request.identity.userID.toString, pageNumber.getOrElse(0), numberOfRecords.getOrElse(10)) map { list =>
      Ok(views.html.vehicles.vehicleList("list of vehicles", request.identity, list))
    }
  }

  def createNew = SecuredAction.async { implicit request =>
    Future.successful(Ok(views.html.vehicles.vehicleNew(forms.CreateVehicleForm.form, "create new", request.identity)))
  }

  def details(vehicleID: String) = SecuredAction.async { implicit request =>
    vehicleService.getVehicleDetails(vehicleID) map {
      case Some(o) => Ok(views.html.vehicles.vehicleDetail("details", request.identity, o))
      case None => Ok("Not found")
    }
  }


  override protected def onNotAuthenticated(request: RequestHeader): Option[Future[Result]] = {
    Some(Future.successful(Redirect(routes.ApplicationController.signIn())))
  }


}
