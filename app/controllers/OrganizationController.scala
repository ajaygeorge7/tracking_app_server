package controllers

import java.util.UUID
import javax.inject.Inject

import actors.classifications.MessageBus
import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import forms._
import models.services.{OrganizationService, UserService, VehicleService}
import models.{Organization, User, Vehicle}
import play.api.i18n.MessagesApi
import play.api.mvc.{RequestHeader, Result}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class OrganizationController @Inject()(
                                   val messagesApi: MessagesApi,
                                   messageBus: MessageBus,
                                   val env: Environment[User, CookieAuthenticator],
                                   organizationService: OrganizationService,
                                   vehicleService: VehicleService,
                                   userService: UserService)
  extends Silhouette[User, CookieAuthenticator] {


  def list(pageNumber: Option[Int], numberOfRecords: Option[Int]) = SecuredAction.async { implicit request =>
    organizationService.getOrganizationsForUser(request.identity.userID.toString, pageNumber.getOrElse(0), numberOfRecords.getOrElse(10)) map { list =>
      Ok(views.html.orgs.orgList("list of orgs", request.identity, list))
    }
  }

  def details(orgId: String) = SecuredAction.async { implicit request =>
    organizationService.getOrganizationDetails(orgId) map {
      case Some(o) => Ok(views.html.orgs.orgDetail("details", request.identity, o))
      case None => Ok("Not found")
    }
  }


  override protected def onNotAuthenticated(request: RequestHeader): Option[Future[Result]] = {
    Some(Future.successful(Redirect(routes.ApplicationController.signIn())))
  }


}
