package controllers

import java.util.UUID
import javax.inject.Inject

import actors.WebSocketDashboardActor
import actors.classifications.MessageBus
import com.mohiva.play.silhouette.api.exceptions.ProviderException
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.api.services.AvatarService
import com.mohiva.play.silhouette.api.util.{Clock, PasswordHasher, Credentials}
import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import com.mohiva.play.silhouette.impl.exceptions.IdentityNotFoundException
import com.mohiva.play.silhouette.impl.providers.{CredentialsProvider, SocialProviderRegistry}
import forms._
import models.services.UserService
import models.User
import play.api.Configuration
import play.api.i18n.{Messages, MessagesApi}
import play.api.mvc.{Result, RequestHeader, Action, WebSocket}
import net.ceedubs.ficus.Ficus._
import play.api.Play.current

import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration

class ApplicationController @Inject() (
  val messagesApi: MessagesApi,
  messageBus: MessageBus,
  val env: Environment[User, CookieAuthenticator],
  authInfoRepository: AuthInfoRepository,
  avatarService: AvatarService,
  passwordHasher: PasswordHasher,
  userService: UserService,
  credentialsProvider: CredentialsProvider,
  configuration: Configuration,
  clock: Clock,
  socialProviderRegistry: SocialProviderRegistry)
  extends Silhouette[User, CookieAuthenticator] {

  /**
   * Handles the index action.
   *
   * @return The result to display.
   */
  def index = SecuredAction.async { implicit request =>
    Future.successful(Ok(views.html.home("Home",request.identity)))
  }


  def signIn = Action { implicit request =>
    Ok(views.html.signIn(SignInForm.form, SignUpForm.form, socialProviderRegistry))
  }


  /**
   * Handles the Sign Out action.
   *
   * @return The result to display.
   */
  def signOut = SecuredAction.async { implicit request =>
    val result = Redirect(routes.ApplicationController.index())
    env.eventBus.publish(LogoutEvent(request.identity, request, request2Messages))

    env.authenticatorService.discard(request.authenticator, result)
  }


  //ws

  def dashBoardSocket = WebSocket.acceptWithActor[String, String] { request => out =>
    messageBus.subscribe(out, "admin")
    WebSocketDashboardActor.props(out)
  }



  override protected def onNotAuthenticated(request: RequestHeader): Option[Future[Result]] = {
    Some(Future.successful(Redirect(routes.ApplicationController.signIn())))
  }


}
