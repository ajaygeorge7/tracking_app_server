package controllers.api

import javax.inject.Inject

import com.mohiva.play.silhouette.api.{Environment, LogoutEvent, Silhouette}
import com.mohiva.play.silhouette.impl.authenticators.JWTAuthenticator
import com.mohiva.play.silhouette.impl.providers.SocialProviderRegistry
import models.User
import play.api.i18n.{Messages, MessagesApi}
import play.api.libs.json.Json

import scala.concurrent.Future

/**
  * The basic application controller.
  *
  * @param messagesApi            The Play messages API.
  * @param env                    The Silhouette environment.
  * @param socialProviderRegistry The social provider registry.
  */
class ApplicationController @Inject()(
                                       val messagesApi: MessagesApi,
                                       val env: Environment[User, JWTAuthenticator],
                                       socialProviderRegistry: SocialProviderRegistry)
  extends Silhouette[User, JWTAuthenticator] {

  /**
    * Returns the user.
    *
    * @return The result to display.
    */
  def user = SecuredAction.async { implicit request =>
    Future.successful(
      Ok(Json.obj(
        "success" -> true,
        "data" -> request.identity))
    )
  }

  /**
    * Manages the sign out action.
    */
  def signOut = SecuredAction.async { implicit request =>
    env.eventBus.publish(LogoutEvent(request.identity, request, request2Messages))
    env.authenticatorService.discard(request.authenticator, Ok)
  }

}
