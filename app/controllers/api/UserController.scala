package controllers.api

import javax.inject.Inject

import actors.classifications.MessageBus
import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.impl.authenticators.JWTAuthenticator
import models.{UserWithDetailsDTO, User}
import models.services.{OrganizationService, UserService, VehicleService}
import play.api.i18n.MessagesApi
import play.api.libs.json.Json

import scala.concurrent.ExecutionContext.Implicits.global

class UserController @Inject()(
                                val messagesApi: MessagesApi,
                                messageBus: MessageBus,
                                val env: Environment[User, JWTAuthenticator],
                                organizationService: OrganizationService,
                                vehicleService: VehicleService,
                                userService: UserService)
  extends Silhouette[User, JWTAuthenticator] {


  def details = SecuredAction.async { implicit request =>
    organizationService.getOrganizationsConnectedToUserWithRelationAndVehicles(request.identity.userID.toString) map { o =>
      val userWithDetailsDTO = UserWithDetailsDTO(user = request.identity, orgList = o)
      Ok(Json.obj(
        "success" -> true,
        "data" -> userWithDetailsDTO))
    }
  }


}
