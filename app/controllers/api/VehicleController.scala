package controllers.api

import java.util.UUID
import javax.inject.Inject

import actors.classifications.MessageBus
import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.impl.authenticators.{JWTAuthenticator, CookieAuthenticator}
import forms._
import models.services.{OrganizationService, UserService, VehicleService}
import models.{Organization, User, Vehicle}
import play.api.i18n.MessagesApi
import play.api.libs.json.Json

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class VehicleController @Inject()(
                                   val messagesApi: MessagesApi,
                                   messageBus: MessageBus,
                                   val env: Environment[User, JWTAuthenticator],
                                   organizationService: OrganizationService,
                                   vehicleService: VehicleService,
                                   userService: UserService)
  extends Silhouette[User, JWTAuthenticator] {


  def create = SecuredAction.async { implicit request =>
    CreateVehicleForm.form.bindFromRequest.fold(
      form => Future.successful(BadRequest(views.html.vehicles.vehicleNew(form, form.errorsAsJson.toString(), request.identity))),
      data => {
        val org = organizationService.getOrganizationForUserWithName(request.identity.userID.toString, data.orgName) flatMap {
          case Some(o) =>
            Future.successful(o)
          case None =>
            organizationService.createOrganization(Organization(id = UUID.randomUUID().toString, name = data.orgName), adminUser = request.identity) map (o => o.organization.get) // throw error if not found
        }
        org flatMap { o =>
          vehicleService.createVehicle(Vehicle(UUID.randomUUID().toString, Some(data.name)), o) map { v =>
            Ok(Json.obj(
              "success" -> true,
              "data" -> v))
          }
        }
      }
    )
  }

  def list(pageNumber: Option[Int], numberOfRecords: Option[Int]) = SecuredAction.async { implicit request =>
    vehicleService.getVehiclesForUser(request.identity.userID.toString, pageNumber.getOrElse(0), numberOfRecords.getOrElse(10)) map { list =>
      Ok(Json.obj(
        "success" -> true,
        "data" -> list))
    }
  }


  def listFromOrg(orgID: String, pageNumber: Option[Int], numberOfRecords: Option[Int]) = SecuredAction.async { implicit request =>
    vehicleService.getVehiclesForOrg(orgID, pageNumber.getOrElse(0), numberOfRecords.getOrElse(10)) map { list =>
      Ok(Json.obj(
        "success" -> true,
        "data" -> list))
    }
  }


  def details(orgId: String) = SecuredAction.async { implicit request =>
    vehicleService.getVehicleDetails(orgId) map {
      case Some(o) =>
        Ok(Json.obj(
          "success" -> true,
          "data" -> o))
      case None => Ok(Json.obj(
        "success" -> false,
        "message" -> "Not found"))
    }
  }


}
