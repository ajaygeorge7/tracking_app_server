package controllers.api

import java.util.UUID
import javax.inject.Inject

import actors.classifications.MessageBus
import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.impl.authenticators.JWTAuthenticator
import forms._
import models.services.{OrganizationService, UserService, VehicleService}
import models.{Organization, User, Vehicle}
import play.api.i18n.MessagesApi
import play.api.libs.json.Json

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class OrganizationController @Inject()(
                                        val messagesApi: MessagesApi,
                                        messageBus: MessageBus,
                                        val env: Environment[User, JWTAuthenticator],
                                        organizationService: OrganizationService,
                                        vehicleService: VehicleService,
                                        userService: UserService)
  extends Silhouette[User, JWTAuthenticator] {


  def create = SecuredAction.async(parse.json) { implicit request =>
    request.body.validate[CreateOrgForm.Data].map { data =>
      organizationService.createOrganization(Organization(id = UUID.randomUUID().toString, name = data.name), adminUser = request.identity) map { o =>
        Ok(Json.obj(
          "success" -> true,
          "data" -> o))
      }
    }.recoverTotal {
      case error =>
        Future.successful(BadRequest(Json.obj(
          "success" -> false,
          "message" -> "invalid data")))
    }
  }


  def list(pageNumber: Option[Int], numberOfRecords: Option[Int]) = SecuredAction.async { implicit request =>
    organizationService.getOrganizationsConnectedToUserWithRelation(request.identity.userID.toString, pageNumber.getOrElse(0), numberOfRecords.getOrElse(10)) map { list =>
      Ok(Json.obj(
        "success" -> true,
        "data" -> list))
    }
  }

  def details(orgId: String) = SecuredAction.async { implicit request =>
    organizationService.getOrganizationDetailsBasedOnUser(orgId, request.identity.userID.toString) map {
      case Some(o) =>
        Ok(Json.obj(
          "success" -> true,
          "data" -> o))
      case None => Ok(Json.obj(
        "success" -> false,
        "message" -> "Not found"))
    }
  }


}
