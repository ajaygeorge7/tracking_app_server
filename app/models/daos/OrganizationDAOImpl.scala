package models.daos

import java.util.UUID
import javax.inject.Inject

import com.mohiva.play.silhouette.api.LoginInfo
import models.{OrganizationUser, Organization, User}
import play.api.libs.json.{JsObject, JsString, JsUndefined, Json}
import play.api.libs.ws._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


class OrganizationDAOImpl @Inject()(neo: Neo4J) extends OrganizationDAO {

  override def create(org: Organization, adminUser: User): Future[Option[OrganizationUser]] = {

    val jsonOrg = Json.toJson(org).as[JsObject]

    neo.cypher(
      """
        | MATCH (u:User)
        | WHERE u.userID = {userID}
        | CREATE (o:Organization {props})
        | CREATE (o)-[r:HAS_USER{ role:"admin" }]->(u)
        | RETURN o,r
      """.stripMargin,
      Json.obj(
        "props" -> jsonOrg,
        "userID" -> adminUser.userID
      )).map(parseNeoOrgAndRelation)
  }

  def getOrganizationFromNameConnectedToUser(userId: String, name: String): Future[Option[Organization]] = {
    neo.cypher(
      """
        | MATCH (org:Organization)-[:HAS_USER]->(u)
        | WHERE u.userID = {userID}
        |   AND org.name = {name}
        | RETURN org
        | LIMIT 1
      """.stripMargin, Json.obj(
        "userID" -> userId,
        "name" -> name
      )).map(r => parseNeoOrg(r))
  }

  override def getOrganizationsConnectedToUser(userId: String, pageNumber: Int, numberOfRecords: Int): Future[List[Organization]] = {
    neo.cypher(
      """
        | MATCH (org:Organization)-[:HAS_USER]->(u)
        | WHERE u.userID = {userID}
        | RETURN org
        | SKIP {skipCount}
        | LIMIT {takeCount}
      """.stripMargin, Json.obj(
        "userID" -> userId,
        "skipCount" -> pageNumber * numberOfRecords,
        "takeCount" -> numberOfRecords
      )).map(r => parseNeoOrgList(r))
  }

  override def getOrganizationsConnectedToUserWithRelation(userId: String, pageNumber: Int, numberOfRecords: Int): Future[List[OrganizationUser]] = {
    neo.cypher(
      """
        | MATCH (org:Organization)-[r:HAS_USER]->(u)
        | WHERE u.userID = {userID}
        | RETURN org,r
        | ORDER BY org.name
        | SKIP {skipCount}
        | LIMIT {takeCount}
      """.stripMargin, Json.obj(
        "userID" -> userId,
        "skipCount" -> pageNumber * numberOfRecords,
        "takeCount" -> numberOfRecords
      )).map(r => parseNeoOrgAndRelationList(r))
  }

  override def getOrgDetails(orgId: String): Future[Option[Organization]] = {
    neo.cypher(
      """
        | MATCH (org:Organization)
        | WHERE org.id = {orgId}
        | RETURN org
      """.stripMargin, Json.obj(
        "orgId" -> orgId
      )).map(r => parseNeoOrg(r))
  }

  override def getOrgDetails(orgId: String, userId: String): Future[Option[OrganizationUser]] = {
    neo.cypher(
      """
        | MATCH (org:Organization)-[r:HAS_USER]->(u)
        | WHERE u.userID = {userID} AND org.id = {orgId}
        | RETURN org,r
      """.stripMargin, Json.obj(
        "userID" -> userId,
        "orgId" -> orgId
      )).map(r => parseNeoOrgAndRelation(r))
  }

  def parseNeoOrgList(response: WSResponse): List[Organization] = {
    val a = ((Json.parse(response.body) \ "results") (0) \ "data").as[List[JsObject]]
    val b = a map { ob =>
      (ob \ "row") (0).as[Organization]
    }
    b
  }

  def parseNeoOrgAndRelationList(response: WSResponse): List[OrganizationUser] = {
    val a = ((Json.parse(response.body) \ "results") (0) \ "data").as[List[JsObject]]
    val b = a map { ob =>
      val org = (ob \ "row") (0).as[Organization]
      val rel = (ob \ "row") (1).as[OrganizationUser]
      rel.copy(organization = Some(org), isMember = Some(true))
    }
    b
  }

  def parseNeoOrg(response: WSResponse): Option[Organization] = {
    (((Json.parse(response.body) \ "results") (0) \ "data") (0) \ "row") (0).asOpt[Organization]
  }

  def parseNeoOrgAndRelation(response: WSResponse): Option[OrganizationUser] = {
    val org = (((Json.parse(response.body) \ "results") (0) \ "data") (0) \ "row") (0).asOpt[Organization]
    val rel = (((Json.parse(response.body) \ "results") (0) \ "data") (0) \ "row") (1).asOpt[OrganizationUser]
    rel map (r => r.copy(organization = org, isMember = Some(true)))
  }

}