package models.daos

import java.util.UUID

import com.mohiva.play.silhouette.api.LoginInfo
import models.{OrganizationUser, Organization, User}

import scala.concurrent.Future

/**
  * Give access to the org object.
  */
trait OrganizationDAO {

  def create(org: Organization, adminUser: User): Future[Option[OrganizationUser]]

  def getOrganizationsConnectedToUserWithRelation(userId: String, pageNumber: Int, numberOfRecords: Int): Future[List[OrganizationUser]]

  def getOrganizationsConnectedToUser(userId: String, pageNumber: Int, NumberOfRecords: Int): Future[List[Organization]]

  def getOrganizationFromNameConnectedToUser(userId: String, name: String): Future[Option[Organization]]

  def getOrgDetails(orgId: String): Future[Option[Organization]]

  def getOrgDetails(orgId: String, userId: String): Future[Option[OrganizationUser]]
}
