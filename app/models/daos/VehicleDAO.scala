package models.daos

import models.{Organization, Vehicle, User}

import scala.concurrent.Future

/**
  * Give access to the vehicle object.
  */
trait VehicleDAO {

  def getVehiclesConnectedToUser(userId: String, pageNumber: Int, numberOfRecords: Int): Future[List[Vehicle]]

  def create(vehicle: Vehicle, org: Organization): Future[Option[Vehicle]]

  def getVehiclesConnectedToOrg(orgId: String, pageNumber: Int, NumberOfRecords: Int): Future[List[Vehicle]]

  def getVehicleDetails(vehicleId: String): Future[Option[Vehicle]]
}
