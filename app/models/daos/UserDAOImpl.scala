package models.daos

import java.util.UUID
import javax.inject.Inject

import com.mohiva.play.silhouette.api.LoginInfo
import models.User
import play.api.libs.json.{JsObject, JsString, JsUndefined, Json}
import play.api.libs.ws._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


class UserDAOImpl @Inject()(neo: Neo4J) extends UserDAO {

  /**
    * Finds a user by its login info.
    *
    * @param loginInfo The login info of the user to find.
    * @return The found user or None if no user for the given login info could be found.
    */
  def find(loginInfo: LoginInfo): Future[Option[User]] = {
    neo.cypher(
      """
        | MATCH (u:User)-[:HAS_LOGIN_INFO]->(l)
        | WHERE l.providerID = {providerID} AND
        |       l.providerKey = {providerKey}
        | RETURN u,l
      """.stripMargin, Json.obj(
        "providerID" -> JsString(loginInfo.providerID),
        "providerKey" -> JsString(loginInfo.providerKey)
      )).map(parseNeoUser)
  }

  /**
    * Finds a user by its user ID.
    *
    * @param userID The ID of the user to find.
    * @return The found user or None if no user for the given ID could be found.
    */
  def find(userID: UUID): Future[Option[User]] = {
    neo.cypher(
      """
        | MATCH (n:User)-[:HAS_LOGIN_INFO]->(l)
        | WHERE n.userID = {userID}
        | RETURN n,l
      """.stripMargin, Json.obj(
        "userID" -> userID.toString
      )).map(parseNeoUser)
  }

  /**
    * Saves a user.
    *
    * @param user The user to save.
    * @return The saved user.
    */
  def save(user: User): Future[User] = {

    val jsonUser = Json.toJson(user).as[JsObject] - "loginInfo"
    val jsonLoginInfo = Json.toJson(user.loginInfo).as[JsObject]

    neo.cypher(
      """
        | CREATE (n:User {props})
        | CREATE (n)-[:HAS_LOGIN_INFO]->(o:LoginInfo {loginProps})
        | RETURN n
      """.stripMargin,
      Json.obj(
        "props" -> jsonUser,
        "loginProps" -> jsonLoginInfo
      )).map(response => user)
  }

  /**
    * Parses a WsResponse to get a unique user out of it.
    *
    * @param response response object
    * @return The parsed user.
    */
  def parseNeoUser(response: WSResponse): Option[User] = {
    (((Json.parse(response.body) \ "results") (0) \ "data") (0) \ "row") (0) match {
      case _: JsUndefined => None
      case user => {
        val loginInfo = (((Json.parse(response.body) \ "results") (0) \ "data") (0) \ "row") (1).as[LoginInfo]
        Some(User(
          UUID.fromString((user \ "userID").as[String]),
          loginInfo,
          (user \ "firstName").asOpt[String],
          (user \ "lastName").asOpt[String],
          (user \ "fullName").asOpt[String],
          (user \ "email").asOpt[String],
          (user \ "avatarURL").asOpt[String]
        ))
      }
    }
  }
}