package models.daos

import javax.inject.Inject

import models.{Organization, Vehicle, User}
import play.api.Logger
import play.api.libs.json.{JsObject, Json}
import play.api.libs.ws._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


class VehicleDAOImpl @Inject()(neo: Neo4J) extends VehicleDAO {

  override def create(vehicle: Vehicle, org: Organization): Future[Option[Vehicle]] = {

    val jsonVehicle = Json.toJson(vehicle).as[JsObject]

    neo.cypher(
      """
        | MATCH (o:Organization)
        | WHERE o.id = {orgId}
        | CREATE (v:Vehicle {props})
        | CREATE (o)-[:HAS_VEHICLE]->(v)
        | RETURN v
      """.stripMargin,
      Json.obj(
        "props" -> jsonVehicle,
        "orgId" -> org.id
      )).map(parseNeoVehicle)
  }

  override def getVehiclesConnectedToUser(userID: String, pageNumber: Int, numberOfRecords: Int): Future[List[Vehicle]] = {
    neo.cypher(
      """
        | MATCH (u:User)<-[:HAS_USER]-(o:Organization)-[:HAS_VEHICLE]->(v)
        | WHERE u.userID = {userID}
        | RETURN v
        | SKIP {skipCount}
        | LIMIT {takeCount}
      """.stripMargin, Json.obj(
        "userID" -> userID,
        "skipCount" -> pageNumber * numberOfRecords,
        "takeCount" -> numberOfRecords
      )).map(r => parseNeoVehicleList(r))
  }

  override def getVehiclesConnectedToOrg(orgId: String, pageNumber: Int, numberOfRecords: Int): Future[List[Vehicle]] = {
    neo.cypher(
      """
        | MATCH (o:Organization)-[:HAS_VEHICLE]->(v)
        | WHERE o.id = {orgId}
        | RETURN v
        | SKIP {skipCount}
        | LIMIT {takeCount}
      """.stripMargin, Json.obj(
        "orgId" -> orgId,
        "skipCount" -> pageNumber * numberOfRecords,
        "takeCount" -> numberOfRecords
      )).map(r => parseNeoVehicleList(r))
  }

  override def getVehicleDetails(vehicleId: String): Future[Option[Vehicle]] = {
    neo.cypher(
      """
        | MATCH (vehicle:Vehicle)
        | WHERE vehicle.id = {vehicleId}
        | RETURN vehicle
      """.stripMargin, Json.obj(
        "vehicleId" -> vehicleId
      )).map(r => parseNeoVehicle(r))
  }

  def parseNeoVehicleList(response: WSResponse) = {
    Logger.logger.debug(response.toString)
    val a = ((Json.parse(response.body) \ "results") (0) \ "data").as[List[JsObject]]
    val b = a map { ob =>
      (ob \ "row") (0).as[Vehicle]
    }
    b
  }

  def parseNeoVehicle(response: WSResponse): Option[Vehicle]= {
    (((Json.parse(response.body) \ "results") (0) \ "data") (0) \ "row") (0).asOpt[Vehicle]
  }

}