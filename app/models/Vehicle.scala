package models

import play.api.libs.json.Json

/**
  * Created by ajayg on 10/2/2016.
  */
case class Vehicle(
                    id: String,
                    name: Option[String] =None,
                    registration: Option[String] = None,
                    model: Option[String] = None,
                    organization: Option[Organization] =None
                  )

object Vehicle {
  implicit val jsonFormat = Json.format[Vehicle]

}