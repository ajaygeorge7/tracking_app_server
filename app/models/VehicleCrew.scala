package models

import play.api.libs.json.Json

/**
  * Created by ajayg on 10/2/2016.
  */
case class VehicleCrew(
                        role: String,
                        user: User
                      )


object VehicleCrew {
  implicit val jsonFormat = Json.format[VehicleCrew]

}