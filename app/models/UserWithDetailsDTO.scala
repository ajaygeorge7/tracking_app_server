package models

import play.api.libs.json.Json

/**
  * Created by ajayg on 11/14/2016.
  */
case class UserWithDetailsDTO(
                               val user: User,
                               val orgList: List[OrganizationUser]
                             )

object UserWithDetailsDTO {
  implicit val jsonFormat = Json.format[UserWithDetailsDTO]

}