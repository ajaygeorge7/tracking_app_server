package models.services

import com.google.inject.Inject
import models.daos.VehicleDAO
import models.{VehicleCrew, Organization, Vehicle}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Created by ajayg on 10/5/2016.
  */
class VehicleServiceImpl @Inject()(vehicleDAO: VehicleDAO, userService: UserService) extends VehicleService {

  override def createVehicle(vehicle: Vehicle, org: Organization): Future[Vehicle] = {
    vehicleDAO.create(vehicle, org) map {
      case None => throw new Exception("Error while creating")
      case Some(o) => o
    }
  }

  override def getVehiclesForOrg(orgId: String, pageNumber: Int, numberOfRecords: Int): Future[List[Vehicle]] = {
    vehicleDAO.getVehiclesConnectedToOrg(orgId, pageNumber, numberOfRecords)
  }

  def getVehiclesForUser(userId: String, pageNumber: Int, numberOfRecords: Int): Future[List[Vehicle]] = {
    vehicleDAO.getVehiclesConnectedToUser(userId, pageNumber, numberOfRecords)
  }

  override def getVehicleDetails(vehicleId: String): Future[Option[Vehicle]] = {
    vehicleDAO.getVehicleDetails(vehicleId)
  }

  override def addCrewToVehicle(vehicleID: String, vehicleCrew: VehicleCrew): Future[VehicleCrew] = {
    vehicleDAO.getVehicleDetails(vehicleID) flatMap {
      case Some(v) =>
        userService.getFromID(vehicleCrew.user.userID) map{
          case Some(u) =>
            vehicleCrew
          case None =>
            // create user with the details
          vehicleCrew
        }
      case None =>
        throw new Exception("no vehicle found for id: " + vehicleID)
    }
  }

}
