package models.services

import models.{OrganizationUser, Organization, User}

import scala.concurrent.Future

/**
  * Created by ajayg on 10/5/2016.
  */
trait OrganizationService {

  def getOrganizationDetails(orgId: String): Future[Option[Organization]]

  def getOrganizationDetailsBasedOnUser(orgId: String, userId: String): Future[Option[OrganizationUser]]

  def createOrganization(group: Organization, adminUser: User): Future[OrganizationUser]

  def getOrganizationsForUser(userId: String, pageNumber: Int, NumberOfRecords: Int): Future[List[Organization]]

  def getOrganizationsConnectedToUserWithRelation(userId: String, pageNumber: Int, numberOfRecords: Int): Future[List[OrganizationUser]]

  def getOrganizationsConnectedToUserWithRelationAndVehicles(userId: String): Future[List[OrganizationUser]]

  def getOrganizationForUserWithName(userId: String, name: String): Future[Option[Organization]]
}
