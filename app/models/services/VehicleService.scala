package models.services

import models.{VehicleCrew, Organization, Vehicle, User}

import scala.concurrent.Future

/**
  * Created by ajayg on 10/5/2016.
  */
trait VehicleService {

  def getVehicleDetails(vehicleId: String): Future[Option[Vehicle]]

  def createVehicle(vehicle: Vehicle, org: Organization): Future[Vehicle]

  def getVehiclesForOrg(orgId: String, pageNumber: Int, numberOfRecords: Int): Future[List[Vehicle]]

  def getVehiclesForUser(userId: String, pageNumber: Int, numberOfRecords: Int): Future[List[Vehicle]]

  def addCrewToVehicle(vehicleID: String, vehicleCrew: VehicleCrew): Future[VehicleCrew]
}
