package models.services

import com.google.inject.Inject
import models.{OrganizationUser, User, Organization}
import models.daos.OrganizationDAO

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by ajayg on 10/5/2016.
  */
class OrganizationServiceImpl @Inject()(organizationDAO: OrganizationDAO,
                                        vehicleService: VehicleService) extends OrganizationService {

  override def createOrganization(org: Organization, adminUser: User): Future[OrganizationUser] = {
    organizationDAO.create(org, adminUser) map {
      case None => throw new Exception("Error while creating")
      case Some(o) => o
    }
  }

  override def getOrganizationForUserWithName(userId: String, name: String): Future[Option[Organization]] = {
    organizationDAO.getOrganizationFromNameConnectedToUser(userId, name)
  }

  override def getOrganizationsForUser(userId: String, pageNumber: Int, numberOfRecords: Int): Future[List[Organization]] = {
    organizationDAO.getOrganizationsConnectedToUser(userId, pageNumber, numberOfRecords)
  }

  override def getOrganizationsConnectedToUserWithRelation(userId: String, pageNumber: Int, numberOfRecords: Int): Future[List[OrganizationUser]] = {
    organizationDAO.getOrganizationsConnectedToUserWithRelation(userId, pageNumber, numberOfRecords)
  }

  override def getOrganizationsConnectedToUserWithRelationAndVehicles(userId: String): Future[List[OrganizationUser]] = {
    organizationDAO.getOrganizationsConnectedToUserWithRelation(userId, 0, Integer.MAX_VALUE) flatMap { f =>
      val b = f map { g =>
        val listOfVehicle = g.organization match {
          case Some(x) => {
            // TODO get gehicles based on relation
            vehicleService.getVehiclesForOrg(x.id, 0, Integer.MAX_VALUE)
          }
          case None => Future.successful(List.empty)
        }
        listOfVehicle map (f => g.copy(vehicles = Some(f)))
      }
      Future.sequence(b)
    }
  }

  override def getOrganizationDetails(orgId: String): Future[Option[Organization]] = {
    organizationDAO.getOrgDetails(orgId)
  }

  override def getOrganizationDetailsBasedOnUser(orgId: String, userId: String): Future[Option[OrganizationUser]] = {
    organizationDAO.getOrgDetails(orgId, userId) flatMap {
      case Some(orgRel) =>
        val returnVal = Future.successful(Some(orgRel.copy(isMember = Some(true))))




        returnVal
      case None => organizationDAO.getOrgDetails(orgId) map (orgDetail => Some(OrganizationUser(isMember = Some(false), organization = orgDetail)))
    }
  }

}
