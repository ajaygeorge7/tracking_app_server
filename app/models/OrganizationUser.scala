package models

import play.api.libs.json.Json

/**
  * Created by ajayg on 10/2/2016.
  */
case class OrganizationUser(
                             isMember: Option[Boolean] = Some(true),
                             role: String = "",
                             organization: Option[Organization],
                             vehicles: Option[List[Vehicle]] = None
                           )

object OrganizationUser {
  implicit val jsonFormat = Json.format[OrganizationUser]
}
