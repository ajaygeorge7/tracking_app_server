$(function () {


    //declarations and stuff

    var users_data_table_template = $('#users_data_table_template').html();
    var appointments_data_table_template = $('#appointments_data_table_template').html();
    var enterpriseUsers_data_table_template = $('#enterpriseUsers_data_table_template').html();


// authentication

    $("#auth_button").click(function () {
        var email = $("#auth_email").val();
        var password = $("#auth_password").val();
        var rememberMe = $("#auth_remember_me").is(":checked");
        var jsonData = {
            email: email,
            password: password,
            rememberMe: rememberMe
        };


        amplify.request({
            resourceId: "auth#signIn",
            data: JSON.stringify(jsonData),
            success: function (status, data) {
                console.log(data);
                amplify.store("_auth_token", data.token);
            },
            error: function (status, message) {
                console.log(message);
            }
        });


    });


//users

    // user search
    $('#admin_search_input').on('input', function (e) {
        var searchInput = $('#admin_search_input').val();
        console.log(searchInput);
        amplify.publish("adminUsers#list_query", searchInput);

    });
    // search users input
    amplify.subscribe("adminUsers#list_query", function (searchInput) {
        amplify.request("adminUsers#list_request", {query: searchInput}, function (result) {
            amplify.publish("adminUsers#list_result", result.data);
        });
    });
    // search users result
    amplify.subscribe("adminUsers#list_result", function (data) {
        console.log("adminUsers#list_result : " + data);
        $('#usersTable').html(Mustache.render(users_data_table_template, {
            items: data
        }));
    });


//enterpriseUsers

    // user search
    $('#admin_search_input').on('input', function (e) {
        var searchInput = $('#admin_search_input').val();
        console.log(searchInput);
        amplify.publish("adminEnterpriseUsers#list_query", searchInput);

    });
    // search users input
    amplify.subscribe("adminEnterpriseUsers#list_query", function (searchInput) {
        amplify.request("adminEnterpriseUsers#list_request", {query: searchInput}, function (result) {
            amplify.publish("adminEnterpriseUsers#list_result", result.data);
        });
    });
    // search users result
    amplify.subscribe("adminEnterpriseUsers#list_result", function (data) {
        console.log("adminEnterpriseUsers#list_result : " + data);
        $('#enterpriseUsersTable').html(Mustache.render(enterpriseUsers_data_table_template, {
            items: data
        }));
    });


//appointments


    // search appointments
    amplify.subscribe("adminAppointments#list_query", function (searchInput) {
        amplify.request("adminAppointments#list_request", {query: searchInput}, function (result) {
            amplify.publish("adminAppointments#list_result", result.data);
        });
    });

    // appointments result
    amplify.subscribe("adminAppointments#list_result", function (data) {
        console.log("adminAppointments#list_result : " + data);
        $('#appointmentsTable').html(Mustache.render(appointments_data_table_template, {
            items: data
        }));
    });


    //ws

    var ws;
    var wsUrl = $("body").data("ws-url-admin-dashboard");
    if (wsUrl) {
        ws = new WebSocket($("body").data("ws-url-admin-dashboard"));
        ws.onmessage = function (event) {
            var message;
            message = console.log(event.data);
            return console.log(message);
        };
        return $("#msgform").submit(function (event) {
            event.preventDefault();
            console.log($("#msgtext").val());
            ws.send($("#msgtext").val());
            return $("#msgtext").val("");
        });
    }


});
