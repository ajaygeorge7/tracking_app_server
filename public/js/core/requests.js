var my_decoder = function (data, status, xhr, success, error) {
    console.log(xhr);
    if (a.success === true) {
        success(xhr.status, a.data);
    } else {
        error(xhr.status, a.message);
    }
};

amplify.request.define("auth#signIn", "ajax", {
    url: "/api/signIn",
    dataType: "json",
    type: "POST",
    contentType: "application/json",
    decoder: my_decoder,
    beforeSend: function( xhr ) {
        xhr.setRequestHeader( "X-Auth-Token",  amplify.store("_auth_token") );
    }
});

amplify.request.define("adminUsers#list_request", "ajax", {
    url: "/admin/users/list",
    dataType: "json",
    type: "GET",
    contentType: "application/json",
    decoder: my_decoder,
    beforeSend: function( xhr ) {
        xhr.setRequestHeader( "X-Auth-Token",  amplify.store("_auth_token") );
    }
});

amplify.request.define("adminEnterpriseUsers#list_request", "ajax", {
    url: "/admin/enterpriseUsers/list",
    dataType: "json",
    type: "GET",
    contentType: "application/json",
    decoder: my_decoder,
    beforeSend: function( xhr ) {
        xhr.setRequestHeader( "X-Auth-Token",  amplify.store("_auth_token") );
    }
});

amplify.request.define("adminAppointments#list_request", "ajax", {
    url: "/admin/appointments/list",
    dataType: "json",
    type: "GET",
    contentType: "application/json",
    decoder: my_decoder,
    beforeSend: function( xhr ) {
        xhr.setRequestHeader( "X-Auth-Token",  amplify.store("_auth_token") );
    }
});
